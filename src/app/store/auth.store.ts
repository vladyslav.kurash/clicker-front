import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import User from '../model/User.model';

@Injectable({
  providedIn: 'root'
})
export class AuthStore{

  private static LOCAL_STORAGE_USER_KEY = 'clicker__user__name__lck';

  private userSubject = new BehaviorSubject<User>(null);

  constructor() {}

  public getUserSubject(): BehaviorSubject<User> {
    return this.userSubject;
  }

  public initUser(): void {
    const user = localStorage.getItem(AuthStore.LOCAL_STORAGE_USER_KEY);

    if (user) {
      this.userSubject.next(JSON.parse(user));
    }
  }

  public setUser(user: User): void {
    localStorage.setItem(AuthStore.LOCAL_STORAGE_USER_KEY, JSON.stringify(user));

    this.userSubject.next(user);
  }

  public destroyCurrentUser(): void {
    localStorage.removeItem(AuthStore.LOCAL_STORAGE_USER_KEY);

    this.userSubject.next(null);
  }

  public isUserExists(): boolean {
    return this.userSubject.value != null;
  }
}
