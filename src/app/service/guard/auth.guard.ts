import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router, CanLoad, Route } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthStore } from 'src/app/store/auth.store';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate, CanLoad {

  constructor(private authStore: AuthStore, private router: Router) {}

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot):
                Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (this.authStore.isUserExists()) {
      return true;
    }

    this.router.navigateByUrl('/sign-in');
    return false;
  }

  canLoad(route: Route, segments: import('@angular/router').UrlSegment[]):
            boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
    if (this.authStore.isUserExists()) {
      return true;
    }

    this.router.navigateByUrl('/sign-in');
    return false;
  }


}
