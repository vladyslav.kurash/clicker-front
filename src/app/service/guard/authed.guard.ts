import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthStore } from 'src/app/store/auth.store';

@Injectable({
  providedIn: 'root'
})
export class AuthedGuard implements CanActivate {

  constructor(private authStore: AuthStore, private router: Router) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      if (this.authStore.isUserExists()) {
        this.router.navigateByUrl('/');
        return false;
      }

      return true;
  }

}
