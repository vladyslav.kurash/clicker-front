import { Injectable } from '@angular/core';
import { AuthStore } from '../store/auth.store';
import { BehaviorSubject } from 'rxjs';
import User from '../model/User.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private authStore: AuthStore) { }

  public initUser(): void {
    this.authStore.initUser();
  }

  public getUserSubject(): BehaviorSubject<User> {
    return this.authStore.getUserSubject();
  }

  public setUser(user: User): void {
    return this.authStore.setUser(user);
  }

  public destroyCurrentUser(): void {
    this.authStore.destroyCurrentUser();
  }

  public isUserExists(): boolean {
    return this.authStore.isUserExists();
  }
}
