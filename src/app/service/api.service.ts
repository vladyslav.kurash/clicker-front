import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private BASE_URL = 'http://localhost:7777';

  constructor(private http: HttpClient) { }

  public get(path: string): Observable<object> {
    return this.http.get(this.generateUrl(path));
  }

  public post(path: string, data: object): Observable<object> {
    return this.http.post(this.generateUrl(path), data);
  }

  private generateUrl(path: string): string {
    return `${this.BASE_URL}/${path}`;
  }
}
