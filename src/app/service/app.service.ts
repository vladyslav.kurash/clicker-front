import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import User from '../model/User.model';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  constructor(private authService: AuthService) { }

  public initAppData(): void {
    this.authService.initUser();
  }
}
