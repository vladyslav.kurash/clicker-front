import RatingItem from './RatingItem.model';

export default interface Rating {
    ratingList: RatingItem[];
}
