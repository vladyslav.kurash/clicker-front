export default interface RatingItem {
    name?: string;
    score: number;
    date?: Date;
}
