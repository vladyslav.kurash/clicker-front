import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { SignInService } from './sign-in.service';
import User from 'src/app/model/User.model';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {

  signInForm: FormGroup;

  constructor(private signInService: SignInService,
              private formBuilder: FormBuilder,
              private router: Router) { }

  ngOnInit(): void {
    this.initForm();
  }

  initForm(): void {
    this.signInForm = this.formBuilder.group({
      name: ['', [Validators.required]]
    });
  }

  onSubmit(): void {
    if (this.signInForm.invalid) {
      Object.keys(this.signInForm.controls)
        .forEach(controlName => this.signInForm.controls[controlName].markAsTouched());

      return;
    }

    this.signInService.signIn(this.mapFormToUser());
    this.router.navigateByUrl('/');
  }

  private mapFormToUser(): User {
    return {name: this.signInForm.value.name};
  }

}
