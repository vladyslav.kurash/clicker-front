import { Injectable } from '@angular/core';
import { AuthService } from 'src/app/service/auth.service';
import User from 'src/app/model/User.model';

@Injectable({
  providedIn: 'root'
})
export class SignInService {

  constructor(private authService: AuthService) { }

  public signIn(user: User): void {
    this.authService.setUser(user);
  }
}
