import { Component, OnInit } from '@angular/core';
import { HomeService } from '../home.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  name = '';

  constructor(private homeService: HomeService,
              private router: Router) { }

  ngOnInit(): void {
    this.loadUser();
  }

  public loadUser(): void {
    this.name = this.homeService.getCurrentUser().name;
  }

  public signOut(): void {
    this.homeService.signOut();
    this.router.navigateByUrl('/sign-in');
  }

}
