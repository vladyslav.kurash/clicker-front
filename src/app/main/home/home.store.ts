import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import Rating from 'src/app/model/Rating.model';
import RatingItem from 'src/app/model/RatingItem.model';

@Injectable({
  providedIn: 'root'
})
export class HomeStore {

  private static LOCAL_STORAGE_PREVIOUS_TRIES_KEY = 'previous__tries__key__lck';

  private worldwideRating = new BehaviorSubject<Rating>(null);
  private previousTries = new BehaviorSubject<Rating>(null);

  constructor() { }

  public loadPreviousTries(): void {
    const previousTries = localStorage.getItem(HomeStore.LOCAL_STORAGE_PREVIOUS_TRIES_KEY);

    if (previousTries) {
      this.previousTries.next(JSON.parse(previousTries));
      return;
    }

    localStorage.setItem(HomeStore.LOCAL_STORAGE_PREVIOUS_TRIES_KEY, JSON.stringify({ratingList: []}));
    this.previousTries.next({ratingList: []});
  }

  public addPreviousTry(ratingItem: RatingItem): void {
    const previousTries: Rating = JSON.parse(localStorage.getItem(HomeStore.LOCAL_STORAGE_PREVIOUS_TRIES_KEY));

    previousTries.ratingList.unshift(ratingItem);

    this.setPreviousTries(previousTries);
  }

  public getWorldwideRatingSubject(): BehaviorSubject<Rating> {
    return this.worldwideRating;
  }

  public setWorldwideRating(rating: Rating): void {
    this.worldwideRating.next(rating);
  }

  public getPreviousTriesSubject(): BehaviorSubject<Rating> {
    return this.previousTries;
  }

  public setPreviousTries(rating: Rating): void {
    localStorage.setItem(HomeStore.LOCAL_STORAGE_PREVIOUS_TRIES_KEY, JSON.stringify(rating));
    this.previousTries.next(rating);
  }

}
