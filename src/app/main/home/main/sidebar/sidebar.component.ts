import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-home-main-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  @Input() title: string;
  @Input() items: string[];

  constructor() { }

  ngOnInit(): void {
  }

}
