import { Component, OnInit } from '@angular/core';
import { HomeService } from '../../home.service';

@Component({
  selector: 'app-home-main-game-field',
  templateUrl: './game-field.component.html',
  styleUrls: ['./game-field.component.scss']
})
export class GameFieldComponent implements OnInit {

  private gameDurationInSeconds = 10;

  gameInProgress = false;
  played = false;
  relax = false;

  countdown = 10;
  clickCount = 0;
  headerMessage = 'Click on the circle below to play.';

  constructor(private homeService: HomeService) { }

  ngOnInit(): void {

  }

  saveGameRecord(): void {
    const clickCount = this.clickCount;
    this.resetGame();
    this.startRelaxing();

    this.headerMessage = this.homeService.getGameResultMessage(clickCount);

    this.homeService.addWorldwideRating({score: clickCount});
    this.homeService.addPreviousTry({score: clickCount, date: new Date()});
  }

  startGame(): void {
    if (this.relax) {
      return;
    }

    this.gameInProgress = true;
    this.played = true;

    const interval = setInterval(() => {
      this.countdown--;

      if (this.countdown <= 0) {
        clearInterval(interval);
        this.saveGameRecord();
      }
    }, 1000);
  }

  resetGame(): void {
    this.countdown = this.gameDurationInSeconds;
    this.clickCount = 0;
    this.gameInProgress = false;
  }

  startRelaxing(): void {
    this.relax = true;

    setTimeout(() => this.relax = false, 2500);
  }

  increaseClickCount(): void {
    this.clickCount++;
  }

  circleClick(): void {
    if (this.gameInProgress) {
      this.increaseClickCount();
    } else {
      this.startGame();
    }
  }

}
