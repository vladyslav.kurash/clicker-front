import { Component, OnInit } from '@angular/core';
import Rating from 'src/app/model/Rating.model';
import { HomeService } from '../home.service';
import RatingItem from 'src/app/model/RatingItem.model';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-home-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  previousTries: string[] = null;
  worldwideRating: string[] = null;

  constructor(private homeService: HomeService,
              private datePipe: DatePipe) { }

  ngOnInit(): void {
    this.loadData();
  }

  loadData(): void {
    this.homeService.loadRatingData();

    this.homeService.getPreviousTriesSubject().subscribe((rating: Rating) => this.previousTries = this.mapPreviousTries(rating));
    this.homeService.getWorldwideRatingSubject().subscribe((rating: Rating) => this.worldwideRating = this.mapWorldwideRating(rating));
  }

  private mapPreviousTries(rating: Rating): string[] {
    return rating ? rating.ratingList.map((item: RatingItem, index: number) => {
      return `${item.score} score. ${this.datePipe.transform(item.date, 'dd.MM.yyyy HH:mm')}`;
    }) : null;
  }

  private mapWorldwideRating(rating: Rating): string[] {
    return rating ? rating.ratingList.map((item: RatingItem, index: number) => {
      return `${index + 1}: ${item.name} | ${item.score} score.`;
    }) : null;
  }

}
