import { Injectable } from '@angular/core';
import { AuthService } from 'src/app/service/auth.service';
import User from 'src/app/model/User.model';
import { BehaviorSubject } from 'rxjs';

import { HomeStore } from './home.store';
import { ApiService } from '../../service/api.service';
import Rating from 'src/app/model/Rating.model';
import RatingItem from 'src/app/model/RatingItem.model';

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  constructor(private authService: AuthService,
              private homeStore: HomeStore,
              private apiService: ApiService) { }

  public signOut(): void {
    this.authService.destroyCurrentUser();
  }

  public getCurrentUser(): User {
    return this.authService.getUserSubject().value;
  }

  public getWorldwideRatingSubject(): BehaviorSubject<Rating> {
    return this.homeStore.getWorldwideRatingSubject();
  }

  public getPreviousTriesSubject(): BehaviorSubject<Rating> {
    return this.homeStore.getPreviousTriesSubject();
  }

  public loadRatingData(): void {
    this.loadWorldwideRating();
    this.loadPreviousTries();
  }

  public loadWorldwideRating(): void {
    this.apiService.get('ratings').subscribe(
      (rating: Rating) => this.homeStore.setWorldwideRating(rating),
      (error) => console.error(`Some error while fetching rating... \n${error.message}`)
    );
  }

  public loadPreviousTries(): void {
    this.homeStore.loadPreviousTries();
  }

  public addWorldwideRating(ratingItem: RatingItem): void {
    const ratingItemRecord = this.putNameInRatingItem(ratingItem);

    this.apiService.post('ratings', ratingItemRecord).subscribe(
      (rating: Rating) => this.homeStore.setWorldwideRating(rating),
      (error) => console.error(`Some error while adding&fetching rating... \n${error.message}`)
    );
  }

  public addPreviousTry(ratingItem: RatingItem): void {
    const ratingItemRecord = this.putNameInRatingItem(ratingItem);

    this.homeStore.addPreviousTry(ratingItemRecord);
  }

  public getGameResultMessage(score: number): string {
    let message = 'Unfortunately, you did not beat any record :(';

    this.homeStore.getWorldwideRatingSubject().value
                                              .ratingList
                                              .every((ratingItem: RatingItem, index: number) => {
                                                if (score > ratingItem.score) {
                                                  message = `You beat the top ${index + 1}! Congratulations! :)`;
                                                  return false;
                                                }

                                                return true;
                                              });

    return message;
  }

  private putNameInRatingItem(ratingItem: RatingItem): RatingItem {
    return {
      ...ratingItem,
      name: this.authService.getUserSubject().value.name
    };
  }
}
