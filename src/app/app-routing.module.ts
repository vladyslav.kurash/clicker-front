import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from './service/guard/auth.guard';
import { AuthedGuard } from './service/guard/authed.guard';
import { SignInComponent } from './main/sign-in/sign-in.component';


const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'home'
  },
  {
    path: 'home',
    canLoad: [AuthGuard],
    loadChildren: './main/home/home.module#HomeModule'
  },
  {
    path: 'sign-in',
    pathMatch: 'full',
    canActivate: [AuthedGuard],
    component: SignInComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
